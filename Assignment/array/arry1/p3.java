import java.io.*;
class ArryDemo3{

	public static void main(String []args)throws IOException{

		BufferedReader br=new BufferedReader(new InputStreamReader(System.in));

		System.out.println("enter the no of arry:");
		int size=Integer.parseInt(br.readLine());
		int arr[]=new int[size];

		System.out.println("enter the elements:");
		int prod=1;
		for(int i=0;i<arr.length;i++){

			arr[i]=Integer.parseInt(br.readLine());

			if(i%2==1){

				prod=prod*arr[i];
			}

		}
		System.out.println("product of odd index element is:"+prod);
	}
}
