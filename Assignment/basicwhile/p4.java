//4: Write a program to count the Odd digits of the given number.
class WhileDemo{

	public static void main(String [] args){

		int n=942111423;
		int count=0;
		while(n!=0){

			int rem=n%10;
			if(rem%2==1){

				count++;
			}

			n=n/10;

		}
		System.out.println("count of odd digit is:"+count);
	}
}
