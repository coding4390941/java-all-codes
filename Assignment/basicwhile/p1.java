//Program 1: Write a program to print a table of 2

class WhileDemo{

	public static void main(String A[]){

		int n=2;
		int i=1;
		while( i<=10){

			System.out.println(n*i);
			i++;
		}
	}
}
