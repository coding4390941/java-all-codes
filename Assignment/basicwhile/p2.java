//Program 2: Write a program to calculate the factorial of the given number.

class WhileDemo{

	public static void main(String A[]){

		int n=6;
		int rem=1;
		while(n>0){
                 	rem=n*rem;
			n--;
		}
		System.out.println(rem);
	}
}
