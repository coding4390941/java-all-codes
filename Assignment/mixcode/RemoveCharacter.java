

/*5.Write a program to remove the occurrences of a specified letter Character.
Input :
Java Program
character to remove : a
*/


import java.io.*;
class Demo{

	public static void main(String []args)throws IOException{

		BufferedReader br =new BufferedReader(new InputStreamReader(System.in));


		System.out.println("enter string:");

		String str=br.readLine();

		System.out.println("enter charcter to remove from given string:");
		char ch=(char)br.read();

		char arr[]=str.toCharArray();

		for(int i=0;i<arr.length;i++){

			if(arr[i]==ch){

				arr[i]=0 ;
			}
		}

		System.out.println(arr);


	}
}

