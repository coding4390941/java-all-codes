

/*7.Write a Program that takes a number as input from the user
and prints only those digits from that number, which are perfect
divisors of the actual number.
Input: 124
Output: The Perfect Divisor Digits from the Number 124 are 1 2 4
*/

import java.io.*;
class Demo{


public static void main(String []args)throws IOException{

	BufferedReader br=new BufferedReader(new InputStreamReader(System.in));

	System.out.println("enter the no:");

	int num=Integer.parseInt(br.readLine());

	int num1=num;

	System.out.println("the perfect divisor digit from the number "+num+"are  ");
	while(num1!=0){

		int rem = num1%10;

		if(num%rem==0){

			System.out.println(rem+" ");

		}
		num1=num1/10;
	}

}
}
