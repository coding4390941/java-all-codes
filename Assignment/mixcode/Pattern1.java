/*1.Write a Program to Print following Pattern.
note: take rows from user.
A C E G
B D F
C E
D
*/

import java.io.*;
class Demo{

	public static void main(String [] args)throws IOException{


		BufferedReader br =new BufferedReader(new InputStreamReader(System.in));

		System.out.println("enter no of rows");		
		
		int row=Integer.parseInt(br.readLine());

		char ch='A';

		int n=row;
	
		for(int i=1;i<=row;i++){

			char ch1=ch;
			
			for(int j=1;j<=n;j++){
				
				System.out.print(ch1+" ");
				ch1++;
				ch1++;

			}
			n--;
			ch++;
			System.out.println();
		}
	}}
