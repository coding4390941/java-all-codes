/*
 10] Max Odd Sum

  Given an array of integers, check whether there is a subsequence with odd sum and
if yes, then find the maximum odd sum. If no subsequence contains an odd sum,
print -1.

*/

import java.util.*;

class MaxOddSum{

	public static void main(String []args){
		Scanner sc=new Scanner(System.in);
		System.out.println("Enter The Size of Array:");
		int len=sc.nextInt();
		int arr[]=new int[len];
		System.out.println("Enter The Elements:");
		for(int i=0;i<arr.length;i++){
			arr[i]=sc.nextInt();
		}
		int num=maxOddSum(arr);
		System.out.println("Maximum odd Sum: "+num);

		}

	static int maxOddSum(int []arr){
		int maxSum=-1;
	
		for(int i=0;i<arr.length;i++){
			int currentSum=0;
			
			for(int j=0;j<arr.length;j++){
				if(arr[j]>0){
				currentSum=currentSum+arr[j];
				if(currentSum%2==1 && currentSum>maxSum){
					maxSum=currentSum;
				}
				}
			}
		}

		return maxSum;
	}
}

