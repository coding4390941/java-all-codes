
/*
 * Given an array A[] of size n. The task is to find the largest element in it.
 * */

import java.util.*; 
class MaximumElement{

	public static void main(String []args){

		Scanner sc=new Scanner(System.in);

		
		System.out.println("Enter The Size Of Array:");
		int n=sc.nextInt();
		int []arr=new int[n];

		System.out.println("Enter The Elements:");

		for(int i=0;i<arr.length;i++){

			arr[i]=sc.nextInt();
		}
		int maxEle=arr[0];
		for(int i=1;i<arr.length;i++){
		
			if(arr[i]>maxEle){
				maxEle=arr[i];
			}
		}


		System.out.println("Maximum Element is :"+maxEle);
	}
}
