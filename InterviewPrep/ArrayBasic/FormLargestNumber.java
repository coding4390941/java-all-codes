/*] Form largest number from digits
Given an array of numbers from 0 to 9 of size N. Your task is to rearrange elements
of the array such that after combining all the elements of the array, the number
formed is maximum.*/
import java.io.*;

class FormLargestNumber{

	public static void main(String []args)throws IOException{

		BufferedReader br=new BufferedReader(new InputStreamReader(System.in));
		System.out.println("Enter The Size Of Array:");

		int size=Integer.parseInt(br.readLine());

		int arr[]=new int[size];

		System.out.println("Enter the Elements Between 0 to 9:");
		for(int i=0;i<arr.length;i++){

			arr[i]=Integer.parseInt(br.readLine());
		}
		
		for(int j=0;j<arr.length;j++){
			int temp=0;

		for(int i=j+1;i<arr.length;i++){
			if(arr[j]<arr[i]){

				temp=arr[j];
				arr[j]=arr[i];
				arr[i]=temp;
			}
		}
		}
		int sum=0;
	
		for(int i=0;i<arr.length;i++){
		
			sum=sum+arr[i];
			if(i==arr.length-1){
				break;
			}
			sum=sum*10;
		}
			
			System.out.print("Largest Number Form :"+sum);
		

	}
}
