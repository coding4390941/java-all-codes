/*16] Last index of One
Given a string S consisting only '0's and '1's, find the last index of the '1' present in
it.
*/
import java.util.*;
class LastIndexOfOne{

      	static int findLastIndexOfOne(String s) {	
		  for (int i = s.length() - 1; i >= 0; i--) {
            			 if (s.charAt(i) == '1') {
           				return i;
           			  }
        		  }
   	       return -1;
	}
  public static void main(String[] args) {

	  Scanner sc=new Scanner(System.in);
	  System.out.println("Enter the String Consisting only 0's and 1's: ");

  	  String input = sc.next();

    	  int lastIndex = findLastIndexOfOne(input);
        
    
      	  if (lastIndex != -1) {
            System.out.println("Last index of '1' is: " + lastIndex);
      
	  } else {
            System.out.println("No '1' found in the string.");
        }
    }
}
