/*11] Product of maximum in first array and minimum in second

  Given two arrays of A and B respectively of sizes N1 and N2, the task is to
calculate the product of the maximum element of the first array and minimum
element of the second array
*/
import java.util.*;

class ProductOfMaxAndMinArray{

	public static void main(String []args){

		Scanner sc=new Scanner(System.in);

		System.out.println("Enter The Size Of first Array:");
		int len1=sc.nextInt();
		System.out.println("Enter The Size Of second Array:");
		int len2=sc.nextInt();

		int arr1[]=new int [len1];
		int arr2[]=new int [len2];
		
		System.out.println("Enter The Elements of first Array:");

		for(int i=0;i<arr1.length;i++){
			arr1[i]=sc.nextInt();
		}
		System.out.println("Enter The Elements of second Array:");
		for(int i=0;i<arr2.length;i++){
			arr2[i]=sc.nextInt();
		} 
		int max=arr1[0];
		
		for(int i=1;i<arr1.length;i++){
			if(max<arr1[i]){
				max=arr1[i];
			}
		}	
		int min=arr2[0];
		for(int i=1;i<arr2.length;i++){
			if(arr2[i]<min){
				min=arr2[i];
			}
		}
	System.out.println("The Product Of "+max+" And "+min+" is "+max*min);	
	}
}
