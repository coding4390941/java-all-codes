/*
 * 9] Remove an Element at Specific Index from an Array
Given an array of a fixed length. The task is to remove an element at a specific
index from the array.
*/

import java.io.*;
import java.util.*;

class RemoveElement{

	public static void main(String []args){

		Scanner sc = new Scanner(System.in);
		System.out.println("Enter The Size OF Array:");

		int size=sc.nextInt();
		int arr[]=new int[size];

		System.out.println("Enter The Elements:");
		for(int i=0;i<arr.length;i++){
			arr[i]=sc.nextInt();
		}
		System.out.println("Enter The Index To Remove Element:");
		int ind=sc.nextInt();
		if(ind<0 || ind >=arr.length){
			System.out.println("Invalid Index :");
			return;
		}
		int arr2[] = new int[arr.length - 1];

		for(int i=0;i<ind;i++){
			arr2[i]=arr[i];
		}
		for(int i=ind+1;i<arr.length;i++){
			arr2[i - 1]=arr[i];
		}
		System.out.print("New Array Is: ");
		for(int data:arr2){
			System.out.print(" "+data);
		}
		System.out.println();
	}
}
