/*Given a sorted array having N elements, find the indices of the first and last
occurrences of an element X in the given array.
Note: If the number X is not found in the array, return '-1' as an array.

*/

import java.util.*;
class FirstAndLastOccurrences{

	public static void main(String []args){
		Scanner sc=new Scanner(System.in);
		System.out.println("Enter The Size Of Array:");
		int size=sc.nextInt();
		int arr[]=new int[size];
		System.out.println("Enter The Elements Of Array:");
		for(int i=0;i<arr.length;i++){
			arr[i]=sc.nextInt();
		}
		System.out.println("Enter The Element To Find :");
		int num=sc.nextInt();

	
		int occ=0;
		int count=0;
		for(int i=0;i<arr.length;i++){

			if(arr[i]==num){
				 occ=i;
				 count++;
				if(count==1){
				System.out.print("First Occurrence  "+occ);
			}
			}
		}
	if(occ!=0){
		System.out.println(" Last Occrrence  "+occ);
	}else{
	
		System.out.println("-1");
	}
	}
}


