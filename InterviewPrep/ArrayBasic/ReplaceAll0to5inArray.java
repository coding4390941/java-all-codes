/*
	Replace all 0's with 5

	You are given an integer N. You need to convert all zeros of N to 5.
*/
import java.util.*;

class Replace0With5{

	public static void main(String args[]){

		Scanner sc=new Scanner(System.in);

		System.out.println("Enter The Size Of Array:");
		int size=sc.nextInt();

		int arr[]=new int[size];

		System.out.println("Enter The Elements:");
		for(int i=0;i<arr.length;i++){

			arr[i]=sc.nextInt();
			if(arr[i]==0){
				arr[i]=5;
			}
		}
		for(int i=0;i<arr.length;i++){

		System.out.print(arr[i]);
	
		}

	}
}

