
/*27] Count pair sum
Given two sorted arrays(arr1[] and arr2[]) of size M and N of distinct elements.
Given a value Sum. The problem is to count all pairs from both arrays whose sum
is equal to Sum.
Note: The pair has an element from each array.
Example 1:
Input:
M=4, N=4 , Sum = 10
arr1[] = {1, 3, 5, 7}
arr2[] = {2, 3, 5, 8}
Output: 2
Explanation: The pairs are: (5, 5) and (7, 3).
Example 2:
Input
N=4, M=4, sum=51
arr1[] = {1, 2, 3, 4}
arr2[] = {5, 6, 7, 8}
Output: 0
*/

import java.util.*;

class CountPairSum{

	public static void main(String []args){
		Scanner sc=new Scanner(System.in);
		System.out.println("Enter The Size Of 1st Array:");
		int size=sc.nextInt();
		int arr[]=new int[size];
		System.out.println("Enter The Elements of First Array:");
		for(int i=0;i<arr.length;i++){
			arr[i]=sc.nextInt();
		}
		
		System.out.println("Enter The Size Of 2nd Array:");
		int size1=sc.nextInt();
		int arr1[]=new int[size1];
		System.out.println("Enter The Elements of Second Array:");
		for(int i=0;i<arr1.length;i++){
			arr1[i]=sc.nextInt();
		}
		
		System.out.println("enter the Sum:");
		int sum=sc.nextInt();

		int num = countPairOfSum(arr,arr1,sum);
		if(num>0){
			System.out.println("Number of Pair Are:"+num);
		}else{
		System.out.println("No Pair Found");
		}

	}

		static int countPairOfSum(int arr[],int arr1[],int sum){

		int count1=0;
		for(int i=0;i<arr.length;i++){
			int count=0;
			for(int j=0;j<arr1.length;j++){
				if(arr[i]+arr1[j] ==sum){
					count++;
				}
			}
			if(count>0){
				count1+=count;
			}
		}
		return count1;
	}
}







































