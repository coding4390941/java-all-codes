/*
 * 18] Find Subarray with given sum | Set 1 (Non-negative
Numbers)
Given an array arr[] of non-negative integers and an integer sum, find a subarray
that adds to a given sum.
Note: There may be more than one subarray with sum as the given sum, print first
such subarray
*/

import java.util.*;

class FindSubArray{

	public static void main(String[]args){

		Scanner sc=new Scanner(System.in);
		System.out.println("Enter The Size of array:");
		int size=sc.nextInt();
		int arr[]=new int[size];
		System.out.println("Enter the Sum:");
		int sum=sc.nextInt();
		System.out.println("Enter The Elemets (Non-negative):");
		for(int i=0;i<arr.length;i++){
			arr[i]=sc.nextInt();
		}
		int tempSum=0;
		int n=0;
		int firstInd=0;
		
		if(sum>0){
		for(int i=n;i<arr.length;i++){
			if(tempSum==0){
				firstInd=i;			
			}
			if(tempSum<sum){
				tempSum=tempSum+arr[i];
			}
			if(tempSum>sum){
				tempSum=0;
				i=n++;
			}if(tempSum==sum){
				
				System.out.println("Sum Of Elements Between Indices  "+firstInd+" And "+i); 
			       break;	
			} 
			
		}

		}else{
			System.out.println("NO SubArray Found");
		}
	}
}
