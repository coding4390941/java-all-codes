/*
19] Find common elements in three sorted arrays
Given three Sorted arrays in non-decreasing order, print all common elements in
these arrays.*/

import java.util.*;
class FindCommonElement{

	public static void main(String []args){

		Scanner sc=new Scanner(System.in);
		System.out.println("Enter The Size Of 1st Array:");
		int size1=sc.nextInt();
		int arr1[]=new int[size1];
		System.out.println("Enter The Elements Of 1st Array:");
		for(int i=0;i<arr1.length;i++){
			arr1[i]=sc.nextInt();
		}
		System.out.println("Enter The Size Of 2st Array:");
		int size2=sc.nextInt();
		int arr2[]=new int[size2];
		System.out.println("Enter The Elements Of 2nd Array:");
		for(int i=0;i<arr2.length;i++){
			arr2[i]=sc.nextInt();
		}
		
		System.out.println("Enter The Size Of 3rd Array:");
		int size3=sc.nextInt();
		int arr3[]=new int[size3];
		System.out.println("Enter The Elements Of 3rd Array:");
		for(int i=0;i<arr3.length;i++){
			arr3[i]=sc.nextInt();
		}
		
	
		for(int i=0;i<arr1.length;i++){
			
			int count=0;
			int temp=0;
			for(int j=0;j<arr2.length;j++){
				if(arr1[i]==arr2[j]){
					temp=arr1[i];
				}

				for(int k=0;k<arr3.length;k++){
					if(temp==arr3[k]){
						count++;
						//System.out.print(temp);
					}
				}
			}
			if(count>=1){
				
				System.out.print(temp+" ");
			}
			/*else{
				System.out.print("No Common Element");
			}*/
		}
	}
}
