
  //  Reader (Parent class) <-- InputStreamReader <-- FileReader 


import java.io.*;
class ReadFileDemo{

	public static void main(String []args)throws IOException{

		FileReader fr=new FileReader("Amar.txt");

		int data =fr.read();

		while(data !=-1){

			System.out.print((char)data);

			data=fr.read();
		}

		fr.close();
	}
}
