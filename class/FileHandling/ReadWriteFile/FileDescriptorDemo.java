

import java.io.*;

class FileDescDemo{

	public static void main(String []args)throws IOException{

		FileInputStream fis=new FileInputStream("Amar.txt");

		FileDescriptor fd=fis.getFD();
	//	System.out.println(fd); //  java.io.FileDescriptor@4aa298b7
	
		FileReader fr=new FileReader(fd);

		int data =fr.read();
		while(data!=-1){

			System.out.print((char)data);
			data=fr.read();
		}
		fr.close();
		
	}
}
