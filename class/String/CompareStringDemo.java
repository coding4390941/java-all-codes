
import java.io.*;

class CompareStringDemo{

	static int myLength(String str){

		char ch[]=str.toCharArray();
		int count=0;
		for(int i=0;i<ch.length;i++){

			count++;
		}

		return count;
	}	
	public static void main(String [] args)throws IOException{

		BufferedReader br=new BufferedReader(new InputStreamReader(System.in));

		System.out.println("Enter 1st String:");
			String str1=br.readLine();

			System.out.println("Enter 2nd  String:");
			String str2=br.readLine();

			if(myLength(str1)==myLength(str2)){

				System.out.println("Strings are Equal");
			}else{
				System.out.println("Strings are NotEqual");
			}
		}
	}	

