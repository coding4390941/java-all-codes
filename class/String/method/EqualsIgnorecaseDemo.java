

class EqualsIgnoreCaseDemo{

	
	public static void main(String [] args){

		String str="AMAR";
		String str2=" ";
	
		String str3="amar";
		String str4="AMAR";
		// EqualsIgnoreCaseDemo obj=new EqualsIgnoreCaseDemo();
		
		System.out.println(str.compareToIgnoreCase(str2));      // 65
		System.out.println(str.compareToIgnoreCase(str3));	// 0
		System.out.println(str.compareToIgnoreCase(str4));	// 0


	}
}
