

class Parent{

	Parent(){
	System.out.println(this);
         System.out.println("in Parent");
	}

}
class Child extends Parent{

	Child(){
	System.out.println(this);
        System.out.println("in child");
	}
}

class Client{

	public static void main(String []args){

		Child obj=new Child();
//		Parent obj2=new Child();
	
	
	//	Parent obj2=new Parent();
	 //      Child obj2=new Parent();            //  error :Parent cannot be converted to Child
		System.out.println(obj);
	}
}
