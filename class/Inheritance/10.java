

class Parent{

	int x=10;
	static int y=20;

	Parent(){

		System.out.println("in parent constructor");
	}
}

class Child extends Parent{

	int x=1000;
	static int y=2000;

	Child(){


		System.out.println("in child constructor");
	}

	void access(){

		System.out.println(super.x);
		System.out.println(super.y);
		System.out.println(x);
		System.out.println(y);

	}
}

class Client{

	public static void main(String []args){

		Child amar=new Child();
		amar.access();
	}
}
