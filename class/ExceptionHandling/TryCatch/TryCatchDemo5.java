

import java.util.*;
import java.io.*;


class Demo{

	void m1(){

		System.out.println("m1");
	}

	public static void main(String []args){

		Demo obj =new Demo();
		obj.m1();
		obj=null;

		try{

			obj.m1();
		}catch(ArithmeticException ae){

			System.out.println("Exception1");
			ae.getMessage();
			ae.toString();
			ae.printStackTrace();
		}catch(RuntimeException ae){

			System.out.println("Exception2");
		}catch(ArrayIndexoutofBound ae){

			System.out.println("Exception3");
		}catch(Exception ae){

			System.out.println("Exception4");
		}

	}

}
