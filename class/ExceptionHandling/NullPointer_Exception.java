

class NullPointer{

	void m1(){

		System.out.println("in m1");
	}
	void m2(){

		System.out.println("in m2");
	}

	public static void main(String []args){

		NullPointer obj=new NullPointer();

		System.out.println("start main");
		obj.m1();

		obj=null;
		obj.m2();

		System.out.println("end main");
	}
}
