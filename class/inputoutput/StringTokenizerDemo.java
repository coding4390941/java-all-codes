

import java.io.*;
import java.util.*;

class StringTokenizerDemo{

	public static void main(String args[])throws IOException{


		BufferedReader  br = new BufferedReader(new InputStreamReader(System.in));

		System.out.println("Enter MatchInfo,MOM Player,Runs:");

		String info = br.readLine();

		StringTokenizer st =new StringTokenizer(info,",");

		String t1 = st.nextToken();
		String t2 = st.nextToken();
		String t3 = st.nextToken();

		int runs = Integer.parseInt(t3);

		System.out.println("MatchInfo:"+t1);
		System.out.println("MOM Player:"+t2);
		System.out.println("Runs:"+runs);
	}
}
