

class MyThread implements Runnable{

	public void run(){

		System.out.println(Thread.currentThread());
		try{
			Thread.sleep(5000);
		}catch(InterruptedException ie){
			System.out.println(ie.toString());
		}
	}
}

class ThreadGroupDemo{

	public static void main(String []args)throws InterruptedException{

	
		ThreadGroup parentTG=new ThreadGroup("India");

		MyThread obj1 = new MyThread();
		MyThread obj2 = new MyThread();

		Thread t1=new Thread(parentTG,obj1,"Maha");
		Thread t2=new Thread(parentTG,obj2,"Guj");

		t1.start();
		t2.start();

		ThreadGroup childTG=new ThreadGroup(parentTG,"Pakistan");

		MyThread obj3=new MyThread();
		MyThread obj4=new MyThread();

		Thread t3=new Thread(childTG,obj3,"karachi");
		Thread t4=new Thread(childTG,obj4,"lahore");

		t3.start();
		t4.start();

	}
}
