

class MyThread extends Thread{

	MyThread(ThreadGroup tg,String str){
	super(tg,str);
	}

	public void run(){

		System.out.println(Thread.currentThread());
		try{
			Thread.sleep(8000);
		}catch(InterruptedException ie){
		
			System.out.println(ie.toString());
		}
	}
}

class ThreadGroupDemo{

	public static void main(String []args)throws InterruptedException{

		ThreadGroup parentTG=new ThreadGroup("INDIA");
		
		MyThread t1=new MyThread(parentTG,"Maharashtra");
		MyThread t2=new MyThread(parentTG,"Gujrat");
		t1.start();
		t2.start();


		ThreadGroup childTG =new ThreadGroup(parentTG,"Pakistan");

		MyThread t3=new MyThread(childTG,"karachi");
		MyThread t4=new MyThread(childTG,"lahore");
		t3.start();
		t4.start();


		
		ThreadGroup childTG2 =new ThreadGroup(parentTG,"Bangladesh");

		MyThread t5=new MyThread(childTG2,"dhaka");
		MyThread t6=new MyThread(childTG2,"mirpur");
		
		t5.start();
		t6.start();


		//childTG2.interrupt();

		System.out.println(parentTG.activeCount());
		System.out.println(parentTG.activeGroupCount());
	}
}
