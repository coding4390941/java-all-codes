
class MyThread extends Thread{

	public void run(){ //throws InterruptedException{ //error: run() in MyThread cannot implement run() in Runnable;
						      // overridden method does not throw InterruptedException
                                                      // thats why we use try catch block
		for(int i=0;i<10;i++){

			System.out.println("In Run");
			try{
				Thread.sleep(1000);

			}catch(InterruptedException ie){
			}
		}


	}

}
class CreateThreadByThreadclass {

	public static void main(String []args)throws InterruptedException{

		MyThread obj=new MyThread();
		obj.start();

		for(int i=0;i<10;i++){
			System.out.println("In Main");
			Thread.sleep(1000);
		}



	}

}
