

class MyThread extends Thread{

	public void run(){

		System.out.println("In run");

		System.out.println(Thread.currentThread().getName());

		// changing thread name

		// Thread.currentThread().setName("Amar");  //or 
		Thread t=Thread.currentThread();
		t.setName("Amar");
		System.out.println(Thread.currentThread().getName());

	}

/*	public void start(){

		System.out.println("In MyThread Start");
	
		run();
	}*/

}
class ThreadDemo{

	public static void main(String []args)throws InterruptedException{

		MyThread obj=new MyThread();

		obj.start();
		System.out.println(Thread.currentThread().getName());
		obj.sleep(1000);
		System.out.println(obj.getName());
	}
}

