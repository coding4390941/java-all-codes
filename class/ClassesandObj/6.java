

class Core2Web{

	int num=8;
	private String str="amar";

	void disp(){

		System.out.println(num);
		System.out.println(str);
	}
}

class Amar{

	public static void main(String []args){

		Core2Web obj=new Core2Web();

		obj.disp();

		System.out.println(obj.num);
		System.out.println(obj.str);   //error: str has private access in Core2Web
	}
}
