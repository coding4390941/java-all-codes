

class Demo{

	int x=10;
	Demo(){

		System.out.println("in constructor");
	}

	{


		System.out.println("instance block1");
	}
	public static void main(String []args){

		Demo obj=new Demo();


		System.out.println("in main");
	}
	{

		System.out.println("instance block2");
	}
}
/*
 priority of static & instance fields

 1. static variable
 2. static block
 3. static methods

 4.instance variable
 5.instance block
 6.constructor
 7.instance method
 
 
  */
