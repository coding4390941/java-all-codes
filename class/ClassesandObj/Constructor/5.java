

class Demo{

	int x=10;

	Demo(){

//		this(40);	//error: recursive constructor invocation
		System.out.println("in no argu constructor");
	}

	Demo(int x){

		this();
		System.out.println("in para Constructor");
	}

	public static void main(String []args){

		Demo obj=new Demo(40);
	}
}
