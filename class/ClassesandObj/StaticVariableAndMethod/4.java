

class StaticBlock{

	int x=10;
	static int y=20;
	static {


		System.out.println("static block1");
	}

	public static void main(String[] args){


		System.out.println("in main method");
	StaticBlock obj=new StaticBlock();

		System.out.println("x="+obj.x);
	}

	static {


		System.out.println("static block2");
		System.out.println("y="+y);

	}

}
/*  o/p
  static block1
  static block2
  y=20
  in main method
  x =10

*/
