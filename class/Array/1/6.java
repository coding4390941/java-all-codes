//odd and even count 
//
import java.io.*;

class ArrayDemo{

        public static void main(String []args)throws IOException{

                BufferedReader br =new BufferedReader(new InputStreamReader(System.in));

                System.out.println("enter the size of array:");

		int size=Integer.parseInt(br.readLine());
		
		int arr[]=new int[size];

		System.out.println("enter the elements:");

		int even=0;
		int odd=0;

		for(int i=0;i<arr.length;i++){

			arr[i]=Integer.parseInt(br.readLine());
		}
		for(int i =0;i<arr.length;i++){

			if(arr[i]%2==0){

				even++;
			}else{

				odd++;
			}
		}
 			
		System.out.println("odd count is:"+odd);
		System.out.println("even count is:"+even);
	}
	
}


