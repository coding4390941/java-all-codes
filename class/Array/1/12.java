

class ArrayDemo{

	static void fun(int arr[]){

		System.out.println("in fun function");
		for(int x: arr){

			System.out.println(x);
		}
		for(int i=0;i<arr.length;i++){

			arr[i]=arr[i]+50;

		}
	}
	public static void main(String []args){
		int arr[]={50,100,150};
		fun(arr);

		System.out.println("in main function after fun()");
		
		for(int x: arr){

			System.out.println(x);
		}

	
}
}
