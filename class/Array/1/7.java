// passing array as an argument
class ArrayDemo{

	int  fun(int x){
		int val=x+20;

		return val;
	}

        public static void main(String []args){
 
//		fun();//  error  non static method fun() cannot be referenced from static context 

		ArrayDemo obj =new ArrayDemo();
		int ret=obj.fun(10);
	
		System.out.println(ret);
	}
}
