
class ArrayDemo{

	static void fun(int arr1[]){

		for(int x:arr1){

			System.out.println(x);
		}
	}

	public static void main(String []args){

		int arr[]={10,20,30,40};
		for(int x:arr){

			System.out.println(x);
		}
		fun(arr);

	}
}
