import java.io.*;
class SwitchDemo3{

	public static void main(String []args)throws IOException{

		BufferedReader br=new BufferedReader(new InputStreamReader(System.in));
		int x=Integer.parseInt(br.readLine());
		switch(x){

			case 1:
				System.out.println("one");
				break;
			case 2:
			        System.out.println("two");
				break;
			case 3:
			        System.out.println("three");

				break;
			case 4:
			        System.out.println("four");
			        break;
			default:
			        System.out.println("NOMATCH");
			        break;	
		}
	}
}
