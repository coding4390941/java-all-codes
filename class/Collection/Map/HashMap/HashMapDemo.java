
import java.util.*;

class HashMapDemo{

	public static void main(String []args){

		HashSet hs=new HashSet();

		hs.add("Kanha");
		hs.add("Ashish");
		hs.add("Rahul");
		hs.add("Badhe");
		System.out.println(hs);


		HashMap hm = new HashMap();
		hm.put("Kanha","Infosys");
		hm.put("Ashish","Barclays");
		hm.put("Rahul","CarPro");
		hm.put("Badhe","BMC");

		System.out.println(hm);

	}
}
