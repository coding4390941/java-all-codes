

// here we need file in which data is changes file name is friends.propertie  s
import java.util.*;
import java.io.*;

class PropertiesDemo{

	public static void main(String []args)throws IOException{

		Properties obj =new Properties();

		FileInputStream fobj =new FileInputStream("friends.properties");

		obj.load(fobj);

		String name= obj.getProperty("Abhi");
		System.out.println(name);

		obj.setProperty("Akshay","Nimgaon");
		FileOutputStream outobj =new FileOutputStream("friends.properties");

		obj.store(outobj,"Updated By Amar");
	}
}
