
//  Queue(I) -->> BlockingQueue(I)  (Bounded)  -->PriorityBQ(C), ArrayBQ(C),LinkedBQ(C)

import java.util.concurrent.*;
import java.util.*;
class BlockingQueueDemo{

	public static void main(String []args)throws InterruptedException{

		BlockingQueue bq=new PriorityBlockingQueue();

		bq.offer(10);
		bq.offer(20);
		bq.offer(30);

		System.out.println(bq);

		bq.put(40);
		System.out.println(bq);

	        
	}
}
