
// PriorityQueue(C) <-- Queue(I) <-- Collection(I)

import java.util.*;

class QueueDemo{

	public static void main(String []args){

		Queue que=new LinkedList();
		que.offer(10);
		que.offer(20);
		que.offer(50);
		que.offer(40);
		que.offer(30);

		System.out.println(que);
		//que.poll(); // remove 1st element
		//System.out.println(que);

		que.remove(); // remove 1st element //if que {null} NoSuchElementException throw 			
		System.out.println("remove"+que);

		System.out.println("peek"+que.peek());
		
		System.out.println("element"+que.element());

		System.out.println(que);
		
	}
}
