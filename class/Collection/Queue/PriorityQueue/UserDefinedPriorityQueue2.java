
import java.util.*;
class Company {

	String pName;
	int teamSize;
	int duration;
	Company(String pName,int teamSize,int duration){

		this.pName=pName;
		this.teamSize=teamSize;
		this.duration=duration;

	}

	public String toString(){

		return "{"+pName+":"+teamSize+","+duration+"}";
	}
}

class SortBySize implements Comparator{

	public int compare(Object obj1,Object obj2){

		return (((Company)obj1).teamSize)-(((Company)obj2).teamSize);
	}
}

class Client{

	public static void main(String []args){

		PriorityQueue pq=new PriorityQueue(new SortBySize());

		pq.offer(new Company("Webdev",9,2));
		pq.offer(new Company("Java",7,3));
		pq.offer(new Company("Python",5,6));

		System.out.println(pq);

		while (!pq.isEmpty()) {
            	   System.out.println(pq.poll());
        	}

		System.out.println(pq);
	}
}
