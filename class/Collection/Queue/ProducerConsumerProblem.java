
import java.util.*;
import java.util.concurrent.*;


class Producer implements Runnable{

	BlockingQueue bq;
	Producer(BlockingQueue bq){

		this.bq=bq;
	}
	public void run(){

		for(int i=1;i<=10;i++){

			try{
				bq.put(i);
			}catch(InterruptedException ie){}
			System.out.println("Producer"+i);

			try{
				Thread.sleep(1000);
			}catch(InterruptedException ie){}
		}
	}
}

class Consumer implements Runnable{

	BlockingQueue bq;
	Consumer(BlockingQueue bq){

		this.bq=bq;
	}

	public void run(){

		for(int i=1;i<=10;i++){
			try{
				bq.put(i);
			}catch(InterruptedException ie){}

			System.out.println("Consumer"+i);
			try{
				Thread.sleep(7000);
			}catch(InterruptedException ie){}
		}
	}
}

class ProducerConsumer{

	public static void main(String []args){

		BlockingQueue bq=new ArrayBlockingQueue(20);  // o/p depend on this no

		Producer produce=new Producer(bq);
		Consumer consume=new Consumer(bq);

		Thread pThread=new Thread(produce);
		Thread cThread=new Thread(consume);

		pThread.start();
		cThread.start();
	}
}
