

import java.util.*;

  class ArrayListDemo extends ArrayList{

	public static void main(String []args){

		ArrayList  al=new ArrayList();
		
		al.add(10);
		al.add(20.5f);
		al.add("Amar");
		al.add(10);
		al.add(20.5f);

		System.out.println(al);


		System.out.println(al.size());
		System.out.println(al.contains("Amar"));

		System.out.println(al.indexOf(10));

		System.out.println(al.lastIndexOf(10));
	
		System.out.println(al.get(3));
		
		System.out.println(al.set(3,"Magar"));
		//System.out.println(al.get(3));
		
		al.add(3,"NIMGAON");
		System.out.println(al);

		System.out.println(al.remove(2));

		ArrayList al2 = new ArrayList();

		al2.add("Akshay");
		al2.add("Akash");
		al2.add("Rahul");

		System.out.println(al.addAll(al2));
		System.out.println(al);

		System.out.println(al.addAll(2,al2));
		System.out.println(al);

		
		//al.removeRange(2,3);  //  extends the class
		 
		//System.out.println(al);

		Object arr[]=al.toArray();
		
		System.out.println(arr);

		for(Object data:arr){
		
			System.out.print(arr+ " ");
		}
		System.out.println();

		al.clear();

		System.out.println(al);
			
	}
}
