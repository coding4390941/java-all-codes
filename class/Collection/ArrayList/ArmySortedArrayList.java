

import java.util.*;

class Army{
	String soldierName=null;
	String rank=null;
	String regiment=null;

	Army(String soldierName,String rank,String regiment){
		this.soldierName=soldierName;
		this.rank=rank;
		this.regiment=regiment;
	}
	public String toString(){
		return "{"+soldierName+":"+","+rank+","+regiment+"}";
	}
}
	class SortByName implements Comparator <Army>{
		public int compare(Army obj1,Army obj2){
			return obj1.soldierName.compareTo(obj2.soldierName);
		}
	}
 	class SortByRank implements Comparator <Army>{
		public int compare(Army obj1,Army obj2){
			return obj1.rank.compareTo(obj2.rank);
		}
	}
class SortListDemo{
	public static void main(String []args){
		ArrayList<Army> al = new ArrayList<Army>();
		al.add(new Army("GovindRaoKhare","Subedar","Maratha"));
		al.add(new Army("RajeevBharwan","Colonel","Gurkha"));
		al.add(new Army("GopalGurunathBewoor","General","Dogra"));
		System.out.println(al);

		Collections.sort(al,new SortByName());
		System.out.println(al);
		
		Collections.sort(al,new SortByRank());
		System.out.println(al);


	}
}


