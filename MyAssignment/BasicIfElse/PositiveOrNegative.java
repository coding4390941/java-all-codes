

//3: Write a java program, take a number, and print whether it is positive or negative.
 import java.io.*;

 class PositiveOrNegative{

	 public static void main(String []args)throws IOException{

		 BufferedReader br=new BufferedReader(new InputStreamReader(System.in));

		 System.out.println("Enter the Number:");

		 int num=Integer.parseInt(br.readLine());

		 if(num>0){
			 System.out.println(num+" is Positive");
		 }else if(num<0){
			 System.out.println(num+" is Negative");
		 }else{
			 System.out.println(num+ " is equal to 0");
		 }

	 }
 }
