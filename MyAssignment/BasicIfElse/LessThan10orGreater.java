

//Program 2: Write a java program, take a number, and print whether it is less than 10 or greater than 10.

import java.io.*;

class GreaterorLessThan10{

	public static void main(String []args)throws IOException{

		BufferedReader br=new BufferedReader(new InputStreamReader(System.in));

		System.out.println("Enter the Number:");
		int num=Integer.parseInt(br.readLine());

		if(num>10){

			System.out.println(num +" is Greater Than 10");
		}else if(num <10){

			System.out.println(num+ "  is Less Than 10");
		}else{

			System.out.println(num+ "  is equal to 10");
		}
	}
}
